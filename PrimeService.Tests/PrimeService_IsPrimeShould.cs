using NUnit.Framework;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    [TestFixture]
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService;
       
        public PrimeService_IsPrimeShould()
        {
            _primeService = new PrimeService();
        }

        //[Test]
        //public void ReturnFalseGivenValueOf1()
        //{
        //    var result = _primeService.IsPrime(1);

        //    Assert.IsFalse(result, "1 should not be prime");
        //}

        //#region Sample_TestCode
        //[TestCase(-1)]
        //[TestCase(0)]
        //[TestCase(1)]
        //[TestCase(3)]
        //public void ReturnFalseGivenValuesLessThan2(int value)
        //{
        //    var result = _primeService.IsPrime(value);

        //    Assert.IsFalse(result, $"{value} No es menor a 2");
        //}
        //#endregion

        #region Test_Clase_String
        [TestCase("we44")]        
        public void LargoString(string pStringCom)
        {
            var result = _primeService.EsIgualString(pStringCom);

            Assert.IsFalse(result, $"{pStringCom} No es igual a 3");
        }
        #endregion
    }
}
